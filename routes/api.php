<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/************ AUTHENTICATION ************/
Route::group(['prefix' => 'auth'], function () {
  Route::post('/register','App\Http\Controllers\Auth\UserController@register')->name('register.api');
  Route::post('/login','App\Http\Controllers\Auth\UserController@login')->name('login.api');
});


Route::middleware('auth:sanctum')->group(function(){
	/**************  USER  ***************/
	Route::get('/user','App\Http\Controllers\Auth\UserController@userdata')->name('user.api');
	Route::post('/user_update','App\Http\Controllers\Auth\UserController@user_update')->name('user_update.api');
	Route::post('/update_cost','App\Http\Controllers\Resto\RestoController@update_cost')->name('update_cost.api');
	/**************  MENU  ***************/
	Route::post('/add_menu','App\Http\Controllers\Menu\MenuController@store')->name('store.api');
	Route::post('/delete_menu','App\Http\Controllers\Menu\MenuController@delete_menu')->name('delete_menu.api');
	/**************  PHOTO  ***************/
	Route::post('/add_photo','App\Http\Controllers\Photo\PhotoController@store')->name('store.api');
	Route::post('/delete_photo','App\Http\Controllers\Photo\PhotoController@delete_photo')->name('delete_photo.api');
	Route::post('/post_resto','App\Http\Controllers\Resto\RestoController@postResto')->name('postResto.api');
	Route::post('/delete_resto','App\Http\Controllers\Resto\RestoController@delete_resto')->name('delete_resto.api');
	Route::post('/update_vehicle','App\Http\Controllers\Resto\RestoController@update_vehicle')->name('update_vehicle.api');

	/**************  REVIEW  ***************/
	Route::post('/add_review','App\Http\Controllers\Review\ReviewController@postReview')->name('postReview.api');
	Route::post('/get_review_by_user','App\Http\Controllers\Review\ReviewController@get_review_by_user')->name('get_review_by_user.api');	

	
});

/**************  RESTO  ***************/
	Route::post('/get_resto_by_category','App\Http\Controllers\Resto\RestoController@get_resto_by_category')->name('get_resto_by_category.api');
	
	Route::get('/get_all_resto','App\Http\Controllers\Resto\RestoController@get_all_resto')->name('get_all_resto.api');
	Route::post('/get_resto_by_id','App\Http\Controllers\Resto\RestoController@get_resto_by_id')->name('get_resto_by_id.api');

	Route::post('/get_photo_by_id','App\Http\Controllers\Photo\PhotoController@get_photo_by_id')->name('get_photo_by_id.api');

	Route::post('/get_menu_by_id','App\Http\Controllers\Menu\MenuController@get_menu_by_id')->name('get_menu_by_id.api');

	/**************  INFO  ***************/
	Route::post('/add_info','App\Http\Controllers\Info\InfoController@add_info')->name('add_info.api');
	Route::post('/get_info_by_id','App\Http\Controllers\Info\InfoController@get_info_by_id')->name('get_info_by_id.api');

	/**************  REVIEW  ***************/
	Route::post('/get_review_by_id','App\Http\Controllers\Review\ReviewController@get_review_by_id')->name('get_review_by_id.api');	
	
	/**************  CATEGORY  ***************/
	Route::get('/category','App\Http\Controllers\Category\CategoryController@get_category')->name('get_category.api');
