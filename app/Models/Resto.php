<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Resto extends Model
{
	use HasFactory,SoftDeletes;
	// use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'name',
		'address',
		'lattitude',
		'longtitude',
		'timings',
		'vehicle',
		'image',
		'cost',
		'category_id',
		'desc'
	];

 public function photos()
    {
        return $this->belongsTo(Photo::class);
    }

    public function categories()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }



  public function getImageAttribute($image)
    {
        return asset('storage/uploads/' . $image);
    }
     public function reviews()
    {
        return $this->belongsTo(Review::class);
    }
}



