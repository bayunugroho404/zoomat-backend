<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;

	protected $fillable = [
		'name',
		'resto_id',
	];

	 public function restos()
    {
        return $this->hasOne(Resto::class,'id');
    }

  public function getNameAttribute($name)
    {
        return asset('storage/uploads/' . $name);
    }

}
