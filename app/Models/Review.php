<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $fillable = [
		'rate',
		'user_id',
		'name',
		'resto_id',
	];



	 public function users()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

	 public function restos()
    {
        return $this->hasOne(Resto::class,'id','resto_id');
    }

}
