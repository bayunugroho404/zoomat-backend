<?php

namespace App\Http\Controllers\Photo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Photo;
use App\Models\Resto;
use Illuminate\Support\Facades\Auth;


class PhotoController extends Controller
{
	public function store(Request $request)
	{
		try {
			if ($file = $request->file('name')) {
				$path =$request->file('name')->store(null, 'uploads');

				$save = new Photo();
				$save->name = $path;
				$save->resto_id= $request->resto_id;
				$save->save();

				return response()->json([
					"success" => true,
					"message" => "File successfully uploaded",
					"file" => $path
				]);

			}


		} catch (\Exception $e) {
			return $e;
		}
	}

	public function get_photo_by_id(Request $request){
		try{
			$data = Photo::where('resto_id',$request->id)->get();
			$resto  = Resto::where('id',$request->id)->first();

			return response()->json([
				'status_code' => 200,
				'resto' => $resto,
				'photos' => $data
			]);
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}
	}
	public function delete_photo(Request $request){
		try
		{
			Photo::where('id', $request->id)->delete();
			return response()->json([
				'status_code' => 200,
				'message' => 'successfully'
			]);
		}catch(Exception $e){
			return response()->json([
				'status_code' => 200,
				'message' => $e
			]);
		}
	}
}
