<?php

namespace App\Http\Controllers\Resto;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ProjectResource;
use App\Models\Resto;
use App\Models\Photo;


use Auth;


class RestoController extends Controller
{

	public function postResto(Request $request){

		try {
			if ($file = $request->file('image')) {
				$path =$request->file('image')->store(null, 'uploads');

				$save = new Resto();
				$save->name = $request->name;
				$save->address = $request->address;
				$save->desc = $request->desc;
				$save->lattitude = $request->lattitude;
				$save->longtitude = $request->longtitude;
				$save->vehicle = $request->vehicle;
				$save->timings = $request->timings;
				$save->category_id = $request->category_id;
				$save->cost = $request->cost;
				$save->image = $path;
				$save->save();

				return response()->json([
					"success" => true,
					"message" => "Data successfully uploaded",
					"file" => $path
				]);

			}


		} catch (\Exception $e) {
			return $e;
		}


		// $data = $request->all();
		// $project = Resto::create($data);
		// return response(['resto' => new ProjectResource($project), 'message' => 'Created successfully'], 200);
	}

	public function get_resto_by_id(Request $request){
		try{
			$data = Resto::where('id',$request->id)->first();
			$photos = Photo::where('resto_id',$data->id)->get();

			return response()->json([
				'status_code' => 200,
				'resto' => $data,
				'photos' => $photos
			]);
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}
	}



	public function get_resto_by_category(Request $request){
		try{
			$data = Resto::where('category_id',$request->category_id)->get();

			return response()->json([
				'status_code' => 200,
				'resto' => $data,
			]);
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}
	}


	public function delete_resto(Request $request){
		try
		{
			Resto::where('id', $request->id)->delete();
			return response()->json([
				'status_code' => 200,
				'message' => 'successfully'
			]);
		}catch(Exception $e){
			return response()->json([
				'status_code' => 200,
				'message' => $e
			]);
		}
	}


	public function get_all_resto(){
		try{
			$data = Resto::get();
			// $photos = Photo::where('resto_id',$data->id)->get();

			return response()->json([
				'status_code' => 200,
				'resto' => $data,
				// 'photos' => $photos
			]);
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}
	}

	
	public function update_vehicle(Request $request){
		try{
			$vehicle = $request->vehicle;

			$res = Resto::find($request->id);
			$res->vehicle = $vehicle;
			$tes =$res->save();

			$response = [
				'message' => 'successfully',
			];
			return $response;
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}

	}

	public function update_cost(Request $request){
		try{
			$cost = $request->cost;

			$res = Resto::find($request->id);
			$res->cost = $cost;
			$tes =$res->save();

			$response = [
				'message' => 'successfully',
			];
			return $response;
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}

	}

}

