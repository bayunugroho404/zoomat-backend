<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function get_category(){
        try{
            $data = Category::with('restos')->get();

            return response()->json([
                'status_code' => 200,
                'resto' => $data,
            ]);
        }catch (\Exception $e) {
            return response()->json([
                'message' => $e,
            ]);
        }
    }
}
