<?php

namespace App\Http\Controllers\Info;

use App\Http\Controllers\Controller;
use App\Models\Info;
use App\Models\Resto;
use Illuminate\Http\Request;

use Auth;

class InfoController extends Controller
{
    public function add_info(Request $request){
		try {
			$user = Info::create($request->toArray());
			return response()->json([
				'status_code' => 200,
				'message' => 'input Successfull',
			]);
		}catch(Exception $error){
			return response()->json([
				'status_code' => 500,
				'message' => 'Error in Registration',
				'error' => $error,
			]);
		}
	}


	public function get_info_by_id(Request $request){
		try{
			$data = Info::where('resto_id',$request->id)->get();
			$resto  = Resto::where('id',$request->id)->first();

			return response()->json([
				'status_code' => 200,
				'resto' => $resto,
				'infos' => $data
			]);
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}
	}
}
