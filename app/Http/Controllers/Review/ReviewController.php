<?php

namespace App\Http\Controllers\Review;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Resto;
use App\Models\Review;
use App\Models\User;
use App\Http\Resources\ProjectResource;

use Auth;
class ReviewController extends Controller
{
	public function postReview(Request $request){
		$data = $request->all();
		$project = Review::create($data);
		return response(['review' => new ProjectResource($project), 'message' => 'Created successfully'], 200);
	}

	public function get_review_by_id(Request $request){
		try{
			$data = Resto::where('id',$request->id)->first();
			$reviews=  Review::with('users')->where('resto_id',$data->id)->get();
			return response()->json([
				'status_code' => 200,
				'resto' => $data,
				'reviews' => $reviews
			]);
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}
	}

	public function get_review_by_user(Request $request){
		try{
			$data =  Review::with('users','restos')->where('user_id',$request->id)->get();
			return response()->json([
				'status_code' => 200,
				'data' => $data,
			]);
		}catch (\Exception $e) {
			return response()->json([
				'message' => $e,
			]);
		}
	}
}
