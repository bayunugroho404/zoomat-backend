<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
               'name' => 'Indonesia',
           ], 
           [
               'name' => 'United States',
           ], 
           [
               'name' => 'Africa',
           ], 
           [
               'name' => 'Bogor',
           ]
    ];

    foreach ($data as $key => $value) {
       Category::create($value);
   }

}
}
